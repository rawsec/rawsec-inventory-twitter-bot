#!/usr/bin/env ruby
# frozen_string_literal: true

# Ruby internal
require 'net/https'
require 'json'
# External
require 'x'
require 'oj'

X_CREDENTIALS = {
  api_key: ENV['twitter_api_key'],
  api_key_secret: ENV['twitter_api_key_secret'],
  access_token: ENV['twitter_access_token'],
  access_token_secret: ENV['twitter_access_token_secret']
}
# Initialize an X API client with OAuth credentials
x_client = X::Client.new(**X_CREDENTIALS)

# API's constant
API_BASE_URL = 'https://inventory.raw.pm/api/'
API_ROOT_ENDPOINT = 'api.json'

# tweet max message size
TWEET_MAX = 280

# API root URI
uri = URI(API_BASE_URL + API_ROOT_ENDPOINT)
# Get the JSON
res = Net::HTTP.get_response(uri)

if res.is_a?(Net::HTTPSuccess)
  # prepare for parsing
  doc = Oj::Doc.open(res.body)
  # parse (JSON string to ruby object)
  json_obj = doc.fetch('/')
  # get categories
  categories = json_obj.keys
  # pseudo-randomly pick a category
  category = categories.sample
  # get sub categories
  json_obj = doc.fetch("/#{category}")
  sub_categories = json_obj.keys
  # pseudo-randomly pick a sub category
  sub_category = sub_categories.sample
  # get item type and number of items
  item_type = category
  item_number = doc.fetch("/#{category}/#{sub_category}/items")
  # pseudo-randomly pick a tool or resource
  prng = Random.new
  prn = prng.rand(item_number) + 1
  picked_item = doc.fetch("/#{category}/#{sub_category}/#{item_type}/#{prn}")

  # format output (long format)
  message = "Tool / ressource of the day\n\n"
  picked_item.each do |key, value|
    if value.is_a?(String)
      message += "#{key}: #{value}\n"
    else
      message += "#{key}: \n"
      value.each do |item|
        message += "  - #{item.values.first}\n"
      end
    end
  end

  cropped = "\n[cropped]\n"
  reference = "\nFind out more on #{API_BASE_URL.chomp('api/')}"
  # maximum cropped message size
  c_max = TWEET_MAX - reference.length - cropped.size
  # maximum cropped message + cropped tag size
  ct_max = TWEET_MAX - reference.length
  # check if message is smaller than twitter max allowed size
  # keep room for the reference and cropped tag
  message = message[0..c_max] + cropped if message.length > ct_max

  # make reference
  message += reference

  # tweet the message
  if ENV['TWITTER_BOT_ENV'] == 'tweet'
    body = { 'text': message }.to_json
    x_client.post('tweets', body)
  elsif ENV['TWITTER_BOT_ENV'] == 'test'
    puts message
  else
    raise 'No environment defined'
  end
end
